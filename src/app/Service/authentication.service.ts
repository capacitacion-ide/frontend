import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private httpClient:HttpClient) { }

  authenticate(username:any, password:any){
    return this.httpClient.post<any>('http://172.16.11.121:3000/api/v1/login',{username,password}).pipe(
        map(
          userData =>{
            sessionStorage.setItem('username',username);
            let tokenStr = 'Bearer '+userData.token;
            sessionStorage.setItem('token',tokenStr);
            return userData;
          }
        )
    )
  }
}
