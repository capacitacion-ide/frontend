import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor{

  constructor() { }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if(sessionStorage.getItem('username') && sessionStorage.getItem('token') && req.headers.get("skip")){
      req = req.clone({
        setHeaders:{
          'Content-type' : 'application/json',
          'Authorization': sessionStorage.getItem('token') || ''
        }
      })
    }
    return next.handle(req).pipe(
      catchError(this.manejarError)
    );
  }

  manejarError( error: HttpErrorResponse){
    console.log("errorrr");
    console.warn(error);
    return throwError(error);
}
}
