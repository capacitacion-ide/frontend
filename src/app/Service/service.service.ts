import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Contacto } from '../Contacto/contacto';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  Url = 'http://172.16.11.121:3000/api/';
  headerInterpector = {headers:{skip:"true"}};

  constructor(private http:HttpClient) { }

  listarContacto(){
    return this.http.get<Contacto[]>(this.Url+"v1/contacto",this.headerInterpector);
  }

  enviarContacto(contacto:Contacto){
    return this.http.post<Contacto>(this.Url+"v1/contacto",contacto,this.headerInterpector);
  }

  editarContacto(idcontacto:number,contacto:Contacto){
    return this.http.put(this.Url+"v1/contacto/"+idcontacto,contacto,this.headerInterpector);
  }

  eliminarConctacto(idcontacto:number){
    return this.http.delete(this.Url+"v1/contacto/"+idcontacto,this.headerInterpector);
  }
}
