import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductoComponent } from './inicio/producto/producto.component';
import { LoginComponent } from './inicio/login/login.component';
import { AddComponent } from './inicio/producto/add/add.component';
import { ListarComponent } from './inicio/producto/listar/listar.component';

const routes: Routes = [
  {path:'',component:LoginComponent},
  {
    path: 'producto',
    component: ProductoComponent,
    children:[
        {path:'add-producto',component:AddComponent},
        {path:'listar-producto',component:ListarComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
