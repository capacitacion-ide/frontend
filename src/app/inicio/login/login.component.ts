import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/Service/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  hide = true;
  form: FormGroup;
  constructor(private fb: FormBuilder, private _snackbar: MatSnackBar, private router:Router,private loginserviceA:AuthenticationService) {
    this.form = this.fb.group({
      usuario: ['', Validators.required],
      password: ['', Validators.required]
    })
   }

  ngOnInit(): void {
    this.loginserviceA.authenticate("Daniel","123456").subscribe(data =>{
      console.log(data);
    })
  }

  ingresar(){
    console.log(this.form);
    const usuario = this.form.value.usuario;
    const password = this.form.value.password;
    sessionStorage.setItem("nombre",usuario);
    console.log(usuario);
    console.log(password);

    if((usuario == 'jefferson' && password == "admin123" ) || (usuario == 'maria' && password == "admin123" ) ){
      //Redireccionar al dashboard
      this.router.navigate(["/producto"])
    }else{
      //Mostrar un mensaje de error
      this.error();
    }
  }

  error(){
     this._snackbar.open('Usuario o contraseña ingresado son invalidos','',{
       duration: 5000,
       horizontalPosition: 'center',
       verticalPosition: 'top'
     })
  }

  
  //panelClass: ['mat-toolbar', 'mat-success']

}
