import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Contacto } from 'src/app/Contacto/contacto';
import { ServiceService } from 'src/app/Service/service.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  hide = true;
  contacto: Contacto = new Contacto();
  form: FormGroup;
  numero = Number(localStorage.getItem("numero"));
  constructor(private fb: FormBuilder, private _snackbar: MatSnackBar, private router:Router, private service:ServiceService) {
    this.form = this.fb.group({
      nombre: ['', Validators.required],
      email: ['', Validators.required],
      mensaje: ['', Validators.required],
      fecha: ['', Validators.required]
    })
   }

  ngOnInit(): void {
  }

  error(){
    this._snackbar.open('Usuario o contraseña ingresado son invalidos','',{
      duration: 5000,
      horizontalPosition: 'center',
      verticalPosition: 'top'
    })
 }

 enviarDatos(){
  const nombre = this.form.value.nombre;
  const email = this.form.value.email;
  const mensaje = this.form.value.mensaje;
  const fecha = this.form.value.fecha;
  console.log(nombre,email,mensaje,fecha);

  this.contacto.id = this.numero;
  this.contacto.nombre =nombre;
  this.contacto.email= email;
  this.contacto.mensaje = mensaje;
  this.contacto.fechanac = fecha;

  console.log(this.contacto);

    if(nombre != ''){
      this.service.enviarContacto(this.contacto).subscribe(data=>{
        console.log('Enviado');
        if(data){
          this.envio_mensaje();
          let nuevo_numero = this.numero+1;
          localStorage.setItem("numero",String(nuevo_numero));
          this.router.navigate(["/producto"]);
          window.location.reload();
        }
      })
    }
  }

  envio_mensaje(){
    this._snackbar.open('Contacto registrado correctamente','',{
      duration: 5000,
      horizontalPosition: 'center',
      verticalPosition: 'top'
    })
 }


}
