import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { Contacto } from 'src/app/Contacto/contacto';
import { AuthenticationService } from 'src/app/Service/authentication.service';
import { ServiceService } from 'src/app/Service/service.service';

@Component({
  selector: 'app-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.css']
})
export class ListarComponent implements OnInit  {
  displayedColumns: string[] = ['id','nombre', 'mensaje', 'email', 'fechanac','accion'];
  dataSource:any;

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  contacto : Contacto[]=[];
  contactosEditar :Contacto = new Contacto();
  form: FormGroup;
  mostrarEditar=true;
  numero=0;
  constructor(private _snackbar: MatSnackBar,private fb: FormBuilder,private service:ServiceService, private loginserviceA:AuthenticationService) {
    this.form = this.fb.group({
      nombre: [''],
      email: [''],
      mensaje: [''],
      fecha: ['']
    })
   }

  ngOnInit (): void {
    this.service.listarContacto().subscribe(data =>{
      console.log(data);
      this.contacto = data;
      if(data){
        this.dataSource = new MatTableDataSource<Contacto>(this.contacto);
        this.dataSource.paginator = this.paginator;
        localStorage.setItem("numero",String(9));
      }
    })

  }

  editar(element:any){
    console.log(element.nombre);
    this.mostrarEditar = false;
    //this.form.value.nombre = element.nombre;
    this.numero = element.id;
    let nombre = <HTMLInputElement> document.getElementById("nombre");
    nombre.value = element.nombre;
    this.form.value.nombre = element.nombre;
    this.contactosEditar.id=element.id;
    let email = <HTMLInputElement> document.getElementById("email");
    email.value = element.email;
    this.form.value.email = element.email;
    this.contactosEditar.email=element.email;
    let mensaje = <HTMLInputElement> document.getElementById("mensaje");
    mensaje.value = element.mensaje;
    this.form.value.mensaje = element.mensaje;
    this.contactosEditar.mensaje=element.mensaje;
    let fecha = <HTMLInputElement> document.getElementById("fecha");
    fecha.value = this.ConvertDateToStringFormat(element.fechanac);
    this.form.value.fecha = fecha.value;
    this.contactosEditar.fechanac=element.fechanac;
    //console.log(element.fecha)
  }

  ConvertDateToStringFormat(dt:Date):string{
    dt = new Date(dt);
    var day = ("0" + dt.getDate()).slice(-2);
    var month = ("0" + (dt.getMonth() + 1)).slice(-2);
    return dt.getFullYear() +"-"+ month +"-" + day;
}

  editarContacto(){
    this.contactosEditar.nombre=this.form.value.nombre;
    this.contactosEditar.email=this.form.value.email;
    this.contactosEditar.mensaje=this.form.value.mensaje;
    this.contactosEditar.fechanac=this.form.value.fecha;
    console.log(this.form.value.nombre);
    console.log(this.form.value.email);
    console.log(this.form.value.mensaje);
    console.log(this.form.value.fecha);
    console.log(this.numero);
    console.log(this.contactosEditar);
    this.service.editarContacto(this.numero,this.contactosEditar).subscribe(data =>{

          this._snackbar.open('Contacto actualizado correctamente','',{
          duration: 5000,
          horizontalPosition: 'center',
          verticalPosition: 'top'
        });

        window.location.reload();

    })
  }

  eliminar(element:any){
    this.numero = element.id;
    this.service.eliminarConctacto(this.numero).subscribe(data =>{
      this._snackbar.open('Contacto eliminado correctamente','',{
        duration: 5000,
        horizontalPosition: 'center',
        verticalPosition: 'top',
        panelClass: ['mat-toolbar', 'mat-war']
      });
      window.location.reload();
    })
  }
}






