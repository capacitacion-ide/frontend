import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from 'src/app/Service/service.service';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.css']
})
export class ProductoComponent implements OnInit {

  nombre_usuario = sessionStorage.getItem("nombre");
  constructor(private router:Router, private service:ServiceService) { }

  ngOnInit(): void {

  }

  listar(){
    this.router.navigate(["producto/listar-producto"])
  }

  agregar(){
    this.router.navigate(["producto/add-producto"])
  }

  cerrarSession(){
    this.router.navigate([""])
  }

}
